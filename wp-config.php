<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
//define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', 'C:\xampp\htdocs\htdocs\beckandcall\wp-content\plugins\wp-super-cache/' ); //Added by WP-Cache Manager
define('DB_NAME', 'beckandcall');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '_()k6H&?/{Us5JkPI_9SU4+UnMCon~UU8Nr^(CA+wS|%7)MR:=]wn4Gpq:4w>|:,');
define('SECURE_AUTH_KEY',  'QJ4vjE>[MKlJ6B$S]9(@fHJj8CzDDnl5E]3lE0Oo7zz5l-tf:J&pqJ42NO)mS~:K');
define('LOGGED_IN_KEY',    '[@21e_u5&bJJL-%HP-B:aptOI!sODs@sG[{1@)e4:$`DYL>7{? Nb>Pv>KP#2-M+');
define('NONCE_KEY',        'v5!W6KrwZ{i9AyJs(+f^.p|lTiY#HLuB^#|laQa{fM@9;_up3VIvOP15s~uT<XLK');
define('AUTH_SALT',        'UT1FD[V19x=die(3K`M)ItA$S-pYVx#CM ;2r|-/_vO0:;~F,u#4+oQg ouKFW|B');
define('SECURE_AUTH_SALT', 'shaV<-QcdvoU8u1+dmH&>6)lL1IoL636ALz<{vS7v^-6K)ZhPIv~+Zdm(*q_Bk3~');
define('LOGGED_IN_SALT',   'IO0@wwHJZAU:A|zsDYb|`7gcUx:c!8,?oT6  paW(~r<oU_I1X]~bPh-HeLE9GR[');
define('NONCE_SALT',       '*%|V[a`KOe_:1paSV<:<v @r,2uS}6UWOx_[p.b:m^eUe0(W>c1|@I EV{G{C+s-');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Multisite */
define( 'WP_ALLOW_MULTISITE', true );

define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'localhost');
define('PATH_CURRENT_SITE', '/beckandcall/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
