<?php
/*
Template Name: About
*/
get_header();
the_post();
// Get variables metrodir_Options
//Site options
$opt_metrodir_boxed_version = get_option('opt_metrodir_boxed_version');
//company info
$opt_metrodir_company_email = get_option('opt_metrodir_company_email');
$opt_metrodir_company_phone = get_option('opt_metrodir_company_phone');
$opt_metrodir_company_fax = get_option('opt_metrodir_company_fax');
$opt_metrodir_company_address = get_option('opt_metrodir_company_address');
$opt_metrodir_company_website = get_option('opt_metrodir_company_website');
$opt_metrodir_company_address_lat = get_option('opt_metrodir_company_address_lat');
$opt_metrodir_company_address_lng = get_option('opt_metrodir_company_address_lng');
//company opening
$opt_metrodir_company_1int_day = get_option('opt_metrodir_company_1int_day');
$opt_metrodir_company_1int_hr = get_option('opt_metrodir_company_1int_hr');
$opt_metrodir_company_2int_day = get_option('opt_metrodir_company_2int_day');
$opt_metrodir_company_2int_hr = get_option('opt_metrodir_company_2int_hr');
$opt_metrodir_company_3int_day = get_option('opt_metrodir_company_3int_day');
$opt_metrodir_company_3int_hr = get_option('opt_metrodir_company_3int_hr');
$opt_metrodir_company_4int_day = get_option('opt_metrodir_company_4int_day');
$opt_metrodir_company_4int_hr = get_option('opt_metrodir_company_4int_hr');

//Show Post
global $post;
$args = array(
    'orderby' 		=> 'name',
    'order' 		=> 'DESC',
    'numberposts'     => 0,
    'post_type'       => 'content',
    'suppress_filters' => 0
);
$portfolio_items = get_posts($args);

?>

<!-- Content --><div id="content" class="about-us">

    <!-- Content Inner --><div id="content-inner" class="sidebar-<?php echo $opt_metrodir_sidebar; ?>"><div class="box-container">


            <!-- Content Center --><div id="content-center">

                <div class="project-body body">
                    <?php the_content(); ?>
                </div>

            </div><!-- /Content Center -->

            <div class="clear"></div>

        </div></div><!-- /Content Inner -->


</div><!-- /Content -->




<?php get_template_part ('footer');